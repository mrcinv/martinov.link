# My webpage and blog

The purpose of this blog is to keep notes and stories I would like to share 
with others. 

## What do I expect
The process of writing the blog and notes
 - should be simple to edit and publish
 - should be nice to look at and read
 - should appear on https://martinov.link
 
## How

I will use some sort of static site generator like [Hugo](https://gohugo.io/) 
or [Jekyll](https://jekyllrb.com/) and publish the result on 
[Netlify](https://www.netlify.com/).

